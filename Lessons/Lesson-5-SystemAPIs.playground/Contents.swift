// Lesson 5 - System APIs
// Using built-in system APIs and other functions to do cool things

import Cocoa

// easy things

// get the short name of the user executing the process

let user = NSUserName()
print(user)

// now for the actual user on the system and UID
// note you'll need to import SystemConfiguration to do this

import SystemConfiguration

var uid = uid_t.init(0)

let consoleUser = SCDynamicStoreCopyConsoleUser(nil, &uid, nil)
print("User: \(consoleUser ?? "none" as CFString)")
print("uid: \(uid)")

// Serial number of the machine

let platformExpert: io_service_t = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IOPlatformExpertDevice"))
let platformSerialNumberKey = kIOPlatformSerialNumberKey
let serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert, platformSerialNumberKey as CFString, kCFAllocatorDefault, 0)
let serialNumber = serialNumberAsCFString?.takeUnretainedValue() as! String
print("Serial: \(serialNumber)")

// Some Open Directory functions

import OpenDirectory

let session = ODSession.default()
let node = try ODNode.init(session: session, type: UInt32(kODNodeTypeAuthentication))
let query = try ODQuery.init(node: node, forRecordTypes: kODRecordTypeUsers, attribute: kODAttributeTypeRecordName, matchType: UInt32(kODMatchEqualTo), queryValues: consoleUser, returnAttributes: kODAttributeTypeNativeOnly, maximumResults: 0)

let result = try query.resultsAllowingPartial(false)
let record = result[0] as! ODRecord

print(record.secondsUntilPasswordExpires)

// Note all attribute values are Arrays
print(try record.values(forAttribute: kODAttributeTypeUserShell))

// And now... Keychain
// Most of the Sec* APIs return errors, all real information is via in/outs

var err = OSStatus.init(0)
var myKeychain : SecKeychain?

err = SecKeychainCopyDefault(&myKeychain)

if err == 0 {
    print(myKeychain as Any)
} else {
    print("Ruh roh, no keychain")
}

// and now a bit more practical

var pathLen = UInt32(PATH_MAX)
var pathName: [Int8] = [Int8](repeating: 0, count: Int(PATH_MAX))

err = SecKeychainGetPath(myKeychain, &pathLen, &pathName)

let path = URL(fileURLWithFileSystemRepresentation: pathName, isDirectory: false, relativeTo: nil)

print(path)

// Exercises

// 1. Determine the current console user's GID
// 2. Get the home directory of a user via the OD APIs
// 3. Get a list of the current keychains in the user's search path
























/*

// Answers

// Exercise 1

var gid = gid_t.init(0)

let _ = SCDynamicStoreCopyConsoleUser(nil, nil, &gid)
print("gid: \(gid)")

// Exercise 2

print(try record.values(forAttribute: kODAttributeTypeNFSHomeDirectory))

// Exercise 3

var list : CFArray?

err = SecKeychainCopySearchList(&list)

for keychain in list as! [SecKeychain] {
    
    err = SecKeychainGetPath(keychain, &pathLen, &pathName)
    
    let path = URL(fileURLWithFileSystemRepresentation: pathName, isDirectory: false, relativeTo: nil)
    
    print(path)
}

print(list)
 
*/

