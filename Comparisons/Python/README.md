#  Validating detached PKCS7 signatures in Python and Swift

Functions for validating a detached PKCS7 signature on a file. Ideally used for things like validating software update catalogs.

## Python

Original Code: https://gist.github.com/pudquick/5de9a727769b227b3212534defafbe9d

* Makes heavy use of the PyObjC bridge
* Python's handling of in/out variables is cool `result, signer_status, sec_trust, cert_verify_result = check_detached_sig('test.txt', 'test.txt.sig')`
* Lower level calls to read/write files on the disk
* Don't have to declare variables before using them as in/out pointers

## Swift

Original Code: https://gist.github.com/mactroll/41572958d3f9d3216df6da18412cc6f3

* Demonstrates the back and forth you sometimes have to do with C-based native APIs as Swift makes you think a bit before using pointers
* `cssmPerror()` can convert error numbers to vaguely useful text
* Demonstrates returning a tuple from a function

