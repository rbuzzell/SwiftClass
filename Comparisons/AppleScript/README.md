#  Map Drives and Printers

## AppleScript

Original Code: https://github.com/macmule/MapDrivesAndPrintersBasedOnADGroupMembershipOnOSX

* Lots of `do shell script`
* Pipe output via awk and grep to do text manipulation

## Swift

* NSPrinter -- no idea it existed
* OD native APIs are much more complicated than `dscl read ....` but require less clean up afterwards.
