//
//  MapDrivesAndPrintersBasedOnADGroupMembershipOnOSX.swift
//  Swift Class
//
//  Created by Joel Rennich on 6/3/18.
//  Copyright © 2018 Orchard & Grove Inc. All rights reserved.
//

import Foundation
import OpenDirectory

// Some variables

let targetPrinter = "Shaun"
let group = "Printer Admins"

// get current console user

let loggedInUser = NSUserName()

// now get a user record for them

let session = ODSession.default()

// use the Network node so that we know we're dealing with AD directly

let node = try ODNode.init(session: session, type: UInt32(kODNodeTypeNetwork))

let query = try ODQuery.init(node: node, forRecordTypes: kODRecordTypeUsers, attribute: kODAttributeTypeRecordName, matchType: UInt32(kODMatchEqualTo), queryValues: loggedInUser, returnAttributes: kODAttributeTypeNativeOnly, maximumResults: 0)
let result = try query.resultsAllowingPartial(false)

if result.count == 0 {
    // we didn't get a record, something went wrong
    print("Erorr: user not in AD")
    exit(1)
}

let record = result[0] as! ODRecord

// now get attributes for the user

let attrs = try record.recordDetails(forAttributes: [kODAttributeTypeGUID, kODAttributeTypeUniqueID, "dsAttrTypeNative:memberOf", "SMBHome"])

// get list of printer names

let printers = NSPrinter.printerNames

if printers.contains(targetPrinter) {
    print("all good in printer land")
} else {
    // pseudo code to trigger an lpadmin command
    
    //clitask("/usr/sbin/lpadmin -p PRINTERNAME -E -v lpd://PRINTERIP -P PPDLOCATION -L \"PRINTERLOCATION\" -o printer-is-shared=false")
}

